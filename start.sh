#!/bin/bash

# arguments:
# $1 - bot token

CONTAINER_NAME="tg-bot-container";
VOLUME_NAME="tg-bot-volume";

build_image() {
  echo "Step 1. Build image...";
	docker build -t tg-wednesday --build-arg bot_token=$1 .;
}

check_volume() {
  echo "Step 2. Check volume";
	docker volume inspect ${VOLUME_NAME};
	if [[ $? == 1 ]]; then
		docker volume create ${VOLUME_NAME} 2>/dev/null;
	fi
}

run_container() {
  echo "Step 3. Run container...";
	docker container inspect ${CONTAINER_NAME} 2>/dev/null;
	if [[ $? == 0 ]]; then
		docker container stop ${CONTAINER_NAME};
		docker container rm ${CONTAINER_NAME};
	fi
	docker run --restart=always -d --name ${CONTAINER_NAME} -v ${VOLUME_NAME}:/data tg-wednesday;
}

build_image $1
check_volume
run_container