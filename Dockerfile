FROM python:3.6-alpine
COPY src /opt/tg-wednesday/src
COPY config.ini.template /opt/tg-wednesday/config.ini
COPY requirements.txt /opt/tg-wednesday
WORKDIR /opt/tg-wednesday

ARG bot_token=token
RUN sed -i "s/REPLACE_TOKEN/${bot_token}/g" config.ini

RUN apk update
RUN apk add tzdata
RUN apk add musl-dev gcc libffi-dev openssl-dev
RUN pip3 install -r requirements.txt

RUN cp /usr/share/zoneinfo/Europe/Berlin /etc/localtime
RUN echo "Europe/Berlin" > /etc/timezone

CMD ["python", "-m", "src.main"]
