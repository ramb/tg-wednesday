"""
    Module for telegram bot command handlers
"""
from telegram import Update, Bot, Chat, Message

from src import storage
from src.logger import logger
from src.core import admins_only
from src.storage import get_chat_list
from src.videos import get_video_link


@admins_only
def start(_: Bot, update: Update):
    """
        Handle /start
    """
    chat: Chat = update.effective_chat
    msg: Message = update.message

    if storage.add_chat(chat.id):
        msg.reply_text('✅ Chat was added to list')
    else:
        msg.reply_text('⚠ Current chat is already in the list')


def stop(_: Bot, update: Update):
    """
        Handle /stop
    """
    chat: Chat = update.effective_chat
    msg: Message = update.message

    if storage.remove_chat(chat.id):
        msg.reply_text('✅ Chat was removed from list', disable_notification=True)
    else:
        msg.reply_text('⚠ Current chat is not in the list', disable_notification=True)


@admins_only
def list_chats(bot: Bot, update: Update):
    """
        Handle /list
    """
    msg: Message = update.message

    response: str = '⚠ Active chats:\n'

    for current_chat_id in storage.get_chat_list():
        chat = bot.get_chat(current_chat_id)
        response += (
            ('{} {} {}\n'.format(current_chat_id, chat.first_name, chat.last_name))
            if chat.type == Chat.PRIVATE
            else ('{} {}\n'.format(current_chat_id, chat.title))
        )

    msg.reply_text(response)


def chat_id(_: Bot, update: Update):
    """
        Handle /id
    """
    msg: Message = update.message
    chat: Chat = update.effective_chat

    msg.reply_text('Your chat id is: {}'.format(chat.id))


def status(_: Bot, update: Update):
    """
        Handle /status
    """
    msg: Message = update.message
    chat: Chat = update.effective_chat

    if chat.id in storage.get_chat_list():
        msg.reply_text('✅ Chat is in the list')
    else:
        msg.reply_text('❌ Chat is not in the list')


@admins_only
def force(bot: Bot, _: Update):
    """
        Handle /force
    """
    for current_chat_id in get_chat_list():
        bot.send_message(
            current_chat_id, 'It is Wednesday my dudes {}'.format(get_video_link()))


def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, context.error)
