from telegram.ext import Updater, CommandHandler, Dispatcher

from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.cron import CronTrigger

from src import handlers
from src.storage import init_chat_list, get_chat_list
from src.videos import get_video_link
from src.config import get_config


updater: Updater
scheduler: BackgroundScheduler


def send_wednesday():
    for chat_id in get_chat_list():
        updater.bot.send_message(
            chat_id, 'It is Wednesday my dudes {}'.format(get_video_link()))


def setup_scheduler():
    global scheduler
    scheduler = BackgroundScheduler()
    scheduler.start()
    scheduler.add_job(send_wednesday, CronTrigger(day_of_week='wed', hour=7, minute=0))


def setup_handlers(dp: Dispatcher):
    dp.add_handler(CommandHandler('start', handlers.start))
    dp.add_handler(CommandHandler('stop', handlers.stop))
    dp.add_handler(CommandHandler('status', handlers.status))
    dp.add_error_handler(handlers.error)

    # admin handlers
    dp.add_handler(CommandHandler('list', handlers.list_chats))
    dp.add_handler(CommandHandler('id', handlers.chat_id))
    dp.add_handler(CommandHandler('force', handlers.force))


def main():
    global updater

    token: str = get_config()['main']['token']

    init_chat_list()
    setup_scheduler()

    updater = Updater(token)
    dp: Dispatcher = updater.dispatcher
    setup_handlers(dp)

    updater.start_polling()
    updater.idle()


if __name__ == '__main__':
    main()
