import configparser

config: configparser.ConfigParser = None


def get_config() -> configparser.ConfigParser:
    global config
    if not config:
        config = configparser.ConfigParser()
        config.read('config.ini')
    return config
