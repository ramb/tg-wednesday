import json
from typing import List, Callable
from telegram import Update, Bot, Chat, Message, User
from src.config import get_config


def is_admin(chat_id: int) -> bool:
    admins: List[int] = json.loads(get_config()['main']['admins'])
    return chat_id in admins


def admins_only(fn: Callable[[Bot, Update], None]):
    def wrapper(bot: Bot, update: Update):
        chat: Chat = update.effective_chat
        msg: Message = update.message
        user: User = update.effective_user

        if chat.type != Chat.PRIVATE:
            admins: List[int] = \
                [admin.user.id for admin in bot.get_chat_administrators(chat.id)]

            if user.id not in admins:
                msg.reply_text('❌ This action is forbidden '
                               'for users without administrator rights')
                return

        fn(bot, update)

    return wrapper
