from typing import Set, Callable
import pickle
import os


active_chats: Set[int] = set()
STORAGE_FILENAME: str = '/data/db.bin'


def sync_storage(fn: Callable[[int], bool]):
    def wrapper(chat_id: int):
        res = fn(chat_id)

        if res:
            with open(STORAGE_FILENAME, 'wb') as db:
                global active_chats
                pickle.dump(active_chats, db)

        return res

    return wrapper


def init_chat_list():
    if os.path.isfile(STORAGE_FILENAME):
        with open(STORAGE_FILENAME, 'rb') as db:
            global active_chats
            active_chats = pickle.load(db)


@sync_storage
def add_chat(chat_id: int) -> bool:
    global active_chats
    if chat_id in active_chats:
        return False

    active_chats.add(chat_id)

    return True


@sync_storage
def remove_chat(chat_id: int) -> bool:
    if chat_id not in active_chats:
        return False

    active_chats.remove(chat_id)
    return True


def get_chat_list() -> Set[int]:
    return active_chats
